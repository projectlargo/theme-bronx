<?php

function largo_byline( $echo = true ) {
	global $post;
	$values = get_post_custom( $post->ID );
	$authors = ( function_exists( 'coauthors_posts_links' ) && !isset( $values['largo_byline_text'] ) ) ? coauthors_posts_links( null, null, null, null, false ) : largo_author_link( false );

	$output = sprintf( __('<span class="by-author"><time class="entry-date updated dtstamp pubdate" datetime="%2$s">%3$s</time>', 'largo'),
		$authors,
		esc_attr( get_the_date( 'c' ) ),
		largo_time( false )
	);

	if ( current_user_can( 'edit_post', $post->ID ) )
		$output .=  sprintf( __('<span class="sep"> | </span><span class="edit-link"><a href="%1$s">Edit This Post</a></span>', 'largo'), get_edit_post_link() );

	if ( is_single() && of_get_option( 'clean_read' ) === 'byline' )
		$output .=	__('<a href="#" class="clean-read">View as "Clean Read"</a>', 'largo');

	if ( $echo )
		echo $output;
	return $output;
}

function bronx_header_ad_code() {
	?>
	<script type='text/javascript'>
	var googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];
	(function() {
	var gads = document.createElement('script');
	gads.async = true;
	gads.type = 'text/javascript';
	var useSSL = 'https:' == document.location.protocol;
	gads.src = (useSSL ? 'https:' : 'http:') +
	'//www.googletagservices.com/tag/js/gpt.js';
	var node = document.getElementsByTagName('script')[0];
	node.parentNode.insertBefore(gads, node);
	})();
	</script>
	<script type='text/javascript'>
	googletag.cmd.push(function() {
	googletag.defineSlot('/1291657/Brooklyn_Ad_Slot1_300x250', [300, 250], 'div-gpt-ad-1367765273715-0').addService(googletag.pubads());
	googletag.defineSlot('/1291657/Brooklyn_Ad_Slot2_300x250', [300, 250], 'div-gpt-ad-1367765273715-1').addService(googletag.pubads());
	googletag.defineSlot('/1291657/Brooklyn_Ad_Slot3_300x250', [300, 250], 'div-gpt-ad-1367765273715-2').addService(googletag.pubads());
	googletag.defineSlot('/1291657/Brooklyn_Leaderboard_Ad_Slot', [728, 90], 'div-gpt-ad-1367765273715-3').addService(googletag.pubads());
	googletag.defineSlot('/1291657/Brooklyn_Leaderboard_Lower_Slot', [728, 90], 'div-gpt-ad-1367765273715-4').addService(googletag.pubads());
	googletag.pubads().enableSingleRequest();
	googletag.enableServices();
	});
	</script>
	<?
}
add_action( 'wp_head', 'bronx_header_ad_code' );